package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.List;

public class PyramidBuilder {

    boolean possibility = false;
    int stairs = 0; //
    int blocks = 0;
    int width;
    int height;
    int step = 1;
    
    public int[][] buildPyramid(List<Integer> inputNumbers) throws CannotBuildPyramidException {

        if (inputNumbers.contains(null) || (inputNumbers.size() > 100)) {
            throw new CannotBuildPyramidException();
        } else Collections.sort(inputNumbers);

        for (int i = 0; i<inputNumbers.size(); i++) {
            blocks = blocks + step;
            stairs++;
            if (blocks == inputNumbers.size()) {
                possibility = true;
                break;
            }
            step++;
        }

        int[][] intBuilding;

        if (possibility == true) {
            height = stairs;
            width = stairs * 2 - 1;
            int j = inputNumbers.size() - 1;


            intBuilding = new int[height][width];
            for (int y = height - 1; y >= 0; y--) {
                int count = (height - 1) - y;
                int countZero = count;
                for (int x = width - 1; x >= 0; x--) {
                    if ((x == width - 1 - count) && (x >= countZero)) {
                        intBuilding[y][x] = inputNumbers.get(j);
                        j--;
                        count = count + 2;
                    }
                }
            }

        } else {
            throw new CannotBuildPyramidException();
        }
        return intBuilding;
    }
}



