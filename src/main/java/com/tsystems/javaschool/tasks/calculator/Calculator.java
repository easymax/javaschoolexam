package com.tsystems.javaschool.tasks.calculator;

import java.util.LinkedList;

public class Calculator {

    private boolean mistake = false;

    public String evaluate(String statement) {

        String result = null;
        LinkedList<Float> list = new LinkedList<>();

        try {
            statement = statement.replaceAll(" ", "");
            if (statement.contains(","))
                mistake = true;

            LinkedList<Character> op = new LinkedList<>();
            list = new LinkedList<>();
            int last;

            for (int i = 0; i < statement.length(); i++) {
                char c = statement.charAt(i);
                if (c == '(') {
                    op.add('(');
                } else if (c == ')') {
                    last = op.size() - 1;
                    while (op.get(last) != '(') {
                        System.out.println(op.get(last));
                        processOperation(list, op.get(last));
                        System.out.println(op.get(last));
                        op.remove(op.get(last));
                        last--;
                    }
                    op.remove(last);

                } else if (isOperator(c)) {
                    last = op.size() - 1;
                    while (!op.isEmpty() && priority(op.get(last)) >= priority(c)) {
                        processOperation(list, op.get(last));
                        op.remove(op.get(last));
                        last--;
                    }
                    op.add(c);

                } else if (Character.isDigit(c)) {
                    String operand = "";
                    while (i < statement.length() && (statement.charAt(i) == '.' || Character.isDigit(statement.charAt(i)))) {
                        operand += statement.charAt(i++);
                    }
                    --i;
                    try {
                        list.add(Float.parseFloat(operand));
                    } catch (NumberFormatException e) {
                        mistake = true;
                    }
                }
            }
            while (op.size() > 0) {
                last = op.size() - 1;
                processOperation(list, op.get(last));
                op.remove(op.get(last));
                last--;
            }
        } catch (Exception e) {
            mistake = true;
        }

        try {
            result = String.valueOf(Math.round(list.get(0) * 10000.0) / 10000.0);
            if (result.split("\\.")[1].equals("0")) result = result.substring(0, result.indexOf("."));

        } catch (Exception e) {
            mistake = true;
        }

        if (mistake) {
            result = null;
        }

        return result;
    }


    private int priority(char op) {
        switch (op) {
            case '*':
                return 1;
            case '/':
                return 1;
            case '+':
                return 0;
            case '-':
                return 0;
            default:
                return -1;
        }
    }

    private boolean isOperator(char ch) {
        if (ch == '*' || ch == '/' || ch == '+' || ch == '-') return true;
        else return false;
    }


    public void processOperation(LinkedList<Float> stack, Character operand) {
        float result = 0;
        float first = stack.removeLast();
        float second = stack.removeLast();

        switch (operand) {
            case '+':
                result = second + first;
                break;
            case '-':
                result = second - first;
                break;
            case '*':
                result = first * second;
                break;
            case '/':
                result = second / first;
                if (Float.isInfinite(result)) mistake = true;
                break;
        }

        if (!mistake) {
            stack.add(result);
        } else {
            mistake = true;
        }
    }
}